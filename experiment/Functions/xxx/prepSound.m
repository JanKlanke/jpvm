function prepSound
%
% 2021 by Jan Klanke


global setting
    
% initialize sounds depending in use of DataPixx
if setting.Pixx
   Datapixx('InitAudio');
   Datapixx('RegWrRd');
else
    InitializePsychSound(1);
end
function clearScreenFull 
% this function clears the screen and does this until the screen is refreshed 
% by RS 
 
global scr visual setting 
 
flipped = 0; 
while flipped == 0 
    Screen('FillRect', scr.myimg, visual.bgColor); 
    if setting.Pixx, flipped = PsychProPixx('QueueImage', scr.myimg); 
    else flipped = Screen('Flip', scr.myimg); end
end 

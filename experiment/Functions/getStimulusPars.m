function par = getStimulusPars(num)
%
% 2016 by Martin Rolfs
% 2021 by Jan Klanke

global visual

if nargin < 1
    num = 1;
end

for n = 1:num
    par.amp(n) = .5;               % amplitude; -amp/2:+amp/2, so 1 means full contrast
    par.frq(n) = 4;                % spatial frequency [cycles/deg] 
    par.ori(n) = 0.0*pi;           % orientation [radians] - 0 and pi are vertical, pi/2 and 3*pi/2 are horizontal
    par.pha(n) = 0.0*pi;           % phase [radians]
    par.tap(n) = 1/3;              % size of the 'tapered' section 
    par.siz(n) = 10;               % diameter of the stimulus [deg]
    par.asp(n) = 1;                % aspect ratio of x vs y

    % transform to pixels
    par.frqp(n) = par.frq(n)/visual.ppd;             % spatial frequency [cycles/pix] 
    par.sizp(n) = ceil(par.siz(n)*visual.ppd);       % diameter of the stimulus [pix]
    par.sizp(n) = par.sizp(n) + ~mod(par.sizp(n),2); % diameter of the stimulus corrected to uneven [pix]
end

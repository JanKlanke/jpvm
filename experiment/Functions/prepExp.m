function [vpno, seno, cond, dome, subjectCode, overrideSwitch] = prepExp(experimentName)
%
% 2020 by Jan Klanke
% 
% Input:  experimentName - name of the experiment.
% 
% Output: vpno           - id of the participant
%         seno           - number of the session of the participant
%         cond           - number of the condition/s to be presented
%                          number is supposeed to be overwritten
%         dome           - dominant eye
%         subjectCode    - subject code (experiment name + vpno + seno)
%         overrideSwitch - flag that communicates if default condition
%

FlushEvents('keyDown');
global setting

overrideSwitch = 0;
    
%%%%%%%%%%%%%%%%%%%%%%
% Get participant ID %
%%%%%%%%%%%%%%%%%%%%%%
vpno  = input('>>>> Enter participant ID:  ','s');
while isempty(vpno) 
    % make x default for piloting
    if setting.pilot
        vpno = sprintf('x'); 
    else
        % warn and repeat if its no longer piloting and the experimenter
        % forgot to enter the participant ID properly
        fprintf(1, 'WARNING:  Pls sepcify participant ID by entering a letter or number.\n');
        vpno = input('>>>> Enter participant ID:  ','s');
    end
    % make sure vpno length does not exceed 1 digit
    if length(vpno) > 1
        fprintf(1, 'WARNING:  Subject code too long. Pls only use 1 letter/number.\n');
        vpno = [];
    end
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get session number (real experiment) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if setting.train == 0
    % collect session no and make string w/ leading 0
    seno = input('>>>> Enter session number:  ','s');
    if length(seno) == 1; seno = strcat('0',seno);
    elseif isempty(seno)
        % make "01" default for piloting--warn and repeat if session no is
        % not specified correctly in the main experiment
        if setting.pilot; seno = sprintf('01');
        else
            fprintf(1, 'WARNING:  Pls sepcify session no by entering a number.\n');
            while isempty(seno); seno = input('>>>> Enter session number:  ','s'); end
        end      
    end
    
    % create subject code based on experiment name, vopno, and seno.
    subjectCode = sprintf(strcat(experimentName,'_',vpno,seno));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Get conditions (real experiment)   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % define defaults
    if strcmp(seno, '01')
        defaultConds = [1 2 3];
    else
        defaultConds = [1 4 5]; 
    end
    cond = NaN(1,length(defaultConds));
       
    % default to pre-defined conditions when NOT piloting
    if ~setting.pilot
       cond = defaultConds;
    else
        % ask/repeat asking when piloting
        while numel(intersect(cond, defaultConds)) < numel(cond)
            cond = input('>>>> Enter condition numbers:  ','s');
            % Check whether input is a number.
            if isempty(str2num(cond)) && ~isempty(cond)
                fprintf(1, 'WARNING:  At least one of the conditions you specified does not exist. \nWARNING:  You can normally only choose between conditions: %s\n', num2str(defaultConds));
                continue;
            end
            % Check whether input is set to 'special'.
            if ~isempty(cond); cond = str2num(cond);
            elseif isempty(cond); cond = defaultConds;
            end
            % Check whether conditions (either special or not) are appropriate
            % for session.
            if length(intersect(cond, defaultConds)) < length(cond)
                fprintf(1, 'WARNING:  At least one of the conditions you specified does not exist. \nWARNING:  You can normally only choose between conditions: %s\n',num2str(defaultConds));
                fprintf(1, 'WARNING:  However, I can overwrite the defaults for this session.\n');
                overwrite = input( 'WARNING:  Do you want me to override the session defaults [y / n]? ','s');
                if strcmp(overwrite,'y'); overrideSwitch = 1; break; end
            end
        end
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Default all values (except vpno) for training %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif setting.train == 1
    seno = '01'; 
    subjectCode = sprintf(strcat(experimentName,'p',vpno,seno));
    
    resp = '';
    while ~any(strcmp(resp, {'0', '1', '2'}))
<<<<<<< Updated upstream
        resp = input('>>>> Do you want to practice fixation (0) or saccading (1) or stim. vis. (2):  ','s');
=======
        resp = input('>>>> Do you want to practice fixation (0) or saccding (1) or stim. visibility (2):  ','s');
>>>>>>> Stashed changes
    end
    resp = str2num(resp);
    cond = 6 + resp;
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make sure scale dir is set up correctly  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
defaultSDir = {'Yes -> No', 'No -> Yes'};
dirr = [];
% show pre-defined scale direction
fprintf(1,'>>>> The affixed scale direction is: %s\n', defaultSDir{setting.sdir + 1});
% default to accept when piloting
if setting.pilot
    dirr = input('>>>> Are you sure this is the scale direction you want to use [y / n]?','s');
    if isempty(dirr); dirr = 'y'; end
end
% ask till explicit confirmation is given that pre-defined scale direction
% is correct...
while isempty(dirr) 
    dirr = input('>>>> Are you sure this is the scale direction you want to use [y / n]?','s');
    % ...or change direction if its not
    while dirr ~= 'y' 
        setting.sdir = 1 - setting.sdir;
        fprintf('>>>> So you want the scale direction to be: %s\n', defaultSDir{setting.sdir + 1});
        dirr = input('>>>> Are you sure this is the scale direction you want to use [y / n]?','s');
    end
end
    
%%%%%%%%%%%%%%%%%%%%%%%%
%   Get dominant eye   %
%%%%%%%%%%%%%%%%%%%%%%%%
defaultEye = ['L', 'R', 'B'];
dome = NaN;

% ask for dominant eye 
while length(intersect(dome, defaultEye)) < length(dome) || isempty(dome)
    dome = input('>>>> Enter dominant eye [e.g. L, R, B]:  ','s');
    % repeat question/warn (yadayadayada) when dominant eye is not
    % specified correctly
    if isempty(dome)
        % ...except during piloting b/c it does not matter
        if setting.pilot; dome = 'R'; 
        else fprintf(1, 'WARNING:  Please specify the dominant eye. \nWARNING:  You can choose between the following options: L, R, B\n'); 
        end
    end
    if length(intersect(dome, defaultEye)) < length(dome); fprintf(1, 'WARNING:  You made a mistake when trying to specify the dominant eye. \nWARNING:  You can choose between the following options: L, R, B\n'); end
end
% hand the dominant eye through to settings parameter
if strcmp(dome,'R')
    setting.DOMEYE = 2;
elseif strcmp(dome,'L')
    setting.DOMEYE = 1;
else
    setting.DOMEYE = 2;
    fprintf(1, 'WARNING:  S/th went wrong with specifying the dominant eye. \nWARNING:  I defaultet to the right eye now. Pls abort/restart the script and enter the data carfully.\n');
end
end
 